# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for "waived" property of test cases"""
from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_run_generate

COMMON_YAML = """
                host_types:
                    normal: {}
                recipesets:
                    rcs1:
                      - normal
                arches:
                    - arch
                trees:
                    tree: {}
                template: output.txt.j2
                case:
                  universal_id: test_uid
                  host_types: normal
                  location: somewhere
                  max_duration_seconds: 600
                  maintainers:
                    - name: maint1
                      email: maint1@maintainers.org
"""

OUTPUT_TXT_J2 = """
{%- for scene in SCENES -%}
    {%- for recipeset in scene.recipesets -%}
        {% for HOST in recipeset %}
            {% for test in HOST.tests %}
                {{ test.name }}: {{ test.waived }}
            {% endfor %}
        {% endfor %}
    {%- endfor -%}
{%- endfor -%}
"""


class IntegrationWaivedTests(IntegrationTests):
    """Integration tests for "waived" property of test cases"""

    def test_defaults(self):
        """Check default "waived" values are correct"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A|B): False.*(A|B): False.*'
            )

    def test_booleans_work(self):
        """Check boolean "waived" values work"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                      waived: true
                    B:
                      name: B
                      waived: false
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
            )

    def test_patterns_work(self):
        """Check pattern "waived" values work"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  cases:
                    A:
                      name: A
                      waived:
                        trees: tree
                    B:
                      name: B
                      waived:
                        not:
                          trees: tree
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
            )

    def test_branch_combination(self):
        """Check waived values are combined accross branches correctly"""
        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A|B): False.*(A|B): False.*'
            )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: True
                  cases:
                    A:
                      name: A
                    B:
                      name: B
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A|B): True.*(A|B): True.*'
            )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: True
                  cases:
                    A:
                      name: A
                      waived: False
                    B:
                      name: B
                      waived: False
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A|B): False.*(A|B): False.*'
            )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                      waived: True
                    B:
                      name: B
                      waived: False
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*(A: True|B: False).*(A: True|B: False).*'
            )

        assets = {
            "index.yaml": COMMON_YAML + """
                  waived: False
                  cases:
                    A:
                      name: A
                      waived: True
                      cases:
                        a:
                          name: a
                          waived: False
                    B:
                      name: B
                      waived: False
                      cases:
                        b:
                          name: b
                          waived: True
                """,
            "output.txt.j2": OUTPUT_TXT_J2,
        }
        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                stdout_matching=r'.*A - a: False.*B - b: True.*'
            )
